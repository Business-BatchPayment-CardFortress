#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Business::BatchPayment::CardFortress' ) || print "Bail out!\n";
}

diag( "Testing Business::BatchPayment::CardFortress $Business::BatchPayment::CardFortress::VERSION, Perl $], $^X" );
